# GoAlarm - Ein Notifier für GoMap

Das vorliegende Projekt ist ein von [PokéAlarm](https://github.com/kvangent/pokealarm/) inspiriertes Notifier-System für Pokémon Go-Communities auf Discord und Telegram, dass Daten von [GoMap](https://www.gomap.eu/) nutzt.

## Features

+ Automatisiertes Scrapen der GoMap-Daten
+ Benachrichtigungen auf Telegram und Discord
+ Frei wählbarer Benachrichtigungsinhalt